# Setting up a development environment 

## Things to install before starting

postgres
postgis
redis
ruby (RVM is good for installing the right version)
bundler
NPM

With these installed you can now install the app packages.

in `backend/` run `bundle install` and in `frontend/` run `npm install`

## Things to configire before starting

Both Rails and VueJS need configuration files. The repo contains .example files which you can copy to the same location without the .example and the application will run mostly fine but features like maps will require you to add a mapbox api key.

These files are: 

`backend/config/secrets.yml`
`backend/config/database.yml`
`frontend/.env.local`

With these configured you can now create the database

run `rails db:create && rails db:schema:load`

## Starting the application

Open 3 terminal windows

In window 1 run `cd backend && rails server`
In window 2 run `cd backend && bundle exec sidekiq`
in window 3 run `cd frontend && npm run serve`

You can now visit http://localhost:8080 to see a running verion of PikaTrack

# Contributing to the project

The easiest way to get started is to check the issue tracker for an easy issue and then start working on a change to resolve it. If you get part way through and get stuck feel free to open a merge request showing what you have done and what you got stuck on.