class ConvertTimeToInt < ActiveRecord::Migration[5.2]
  def change
    remove_column :section_efforts, :time
    add_column :section_efforts, :time, :integer
  end
end
