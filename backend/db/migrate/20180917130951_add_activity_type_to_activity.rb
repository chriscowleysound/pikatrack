class AddActivityTypeToActivity < ActiveRecord::Migration[5.2]
    def change
        add_column :activities, :activity_type, :integer, null: false, default: 0
        add_column :sections, :activity_type, :integer, null: false, default: 0
    end
end
