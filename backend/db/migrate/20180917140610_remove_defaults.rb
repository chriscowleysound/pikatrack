class RemoveDefaults < ActiveRecord::Migration[5.2]
  def change
    change_column :activities, :activity_type, :integer, default: nil
    change_column :sections, :activity_type, :integer, default: nil
  end
end
