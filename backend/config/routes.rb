Rails.application.routes.draw do
    require 'sidekiq/web'

    mount_devise_token_auth_for 'User', at: 'auth'
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    mount Sidekiq::Web => '/sidekiq'
    get '/profile' => 'profile#show'
    post '/profile/' => 'profile#update'

    get '/user/:id' => 'user#show'

    get 'section/:id' => 'section#show'
    post 'section/' => 'section#create'

    resources :activity

    get '/feed' => 'feed#index'

    get '/user/:id/activities' => 'activity#index'
end
