process   = Integer(ENV['UNICORN_PROCESS'] || 1)
listen    = Integer(ENV['UNICORN_LISTEN']  || 3000)

worker_processes process

listen listen

