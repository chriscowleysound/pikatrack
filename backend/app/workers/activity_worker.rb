class ActivityWorker
    require 'open-uri'
    require 'path_compare'
    include Sidekiq::Worker

    def perform(activity_id)
        activity = Activity.find(activity_id)
        gpx = GPX::GPXFile.new(gpx_data: open(Rails.application.routes.url_helpers.rails_blob_url(activity.original_activity_log_file)))
        ComputedActivity.create(
            distance: gpx.distance,
            start_time: gpx.tracks.first.segments.first.points.first.time,
            end_time: gpx.tracks[-1].segments[-1].points[-1].time,
            moving_time: gpx.moving_duration,
            activity_log_file: crop_activity(activity.user, gpx),
            elevation: 0,
            activity: activity
        )
        find_section_matches(gpx).each do |section_match|
            SectionEffort.create(activity_id: activity_id, section_id:  section_match[:id], time: section_match[:match_end].time - section_match[:match_start].time)
        end
    end


    # TODO: Clean up below code. What a mess..


    def crop_activity(user, gpx)
        gpx # TODO: Crop activity if in users privacy zone
    end

    def find_section_matches(gpx)
        matches = []
        candidate_matches(gpx) # TODO: Run these matches through path_compare
    end


    def candidate_sections(gpx)
        bounds = find_bounds(gpx)

        candidates = Section.where("ST_Contains(ST_Transform(ST_MakeEnvelope(?,?,?,?,3785),3785), sections.path)",
                                   bounds[:lon_min] - 0.5, bounds[:lat_min] - 0.5, bounds[:lon_min] + 0.5, bounds[:lat_max] + 0.5)
        candidates
    end

    def candidate_matches(gpx)
        candidate_states = []
        candidate_sections(gpx).each do |candidate|
            candidate_states << {
                id: candidate.id,
                path: candidate.path,
                start: candidate.path.points.first,
                end: candidate.path.points.last,
                match_start: nil,
                match_end: nil
            }
        end

        gpx.tracks.first.segments.first.points.each do |point| # TODO: See if this can be done faster
            candidate_states.each_with_index do |candidate, index|
                if candidate_states[index][:match_start] == nil
                    distance = Math.sqrt((candidate[:start].x - point.lon) ** 2) + Math.sqrt((candidate[:start].y - point.lat) ** 2)
                    if distance < 0.0002 # Arbitrary value of closeness. Tweak for best results.
                        candidate_states[index][:match_start] = point
                    end
                elsif candidate_states[index][:match_end] == nil && candidate_states[index][:match_start] != nil
                    distance = Math.sqrt((candidate[:end].x - point.lon) ** 2) + Math.sqrt((candidate[:end].y - point.lat) ** 2)
                    if distance < 0.0002 # Arbitrary value of closeness. Tweak for best results.
                        candidate_states[index][:match_end] = point
                    end
                end
            end
        end

        candidate_states.reject{|c| c[:match_start].nil? || c[:match_end].nil?}
    end

    def find_bounds(gpx)
        bounds = {
            lat_min: gpx.tracks.first.segments.first.points.first.lat,
            lat_max: gpx.tracks.first.segments.first.points.first.lat,
            lon_min: gpx.tracks.first.segments.first.points.first.lon,
            lon_max: gpx.tracks.first.segments.first.points.first.lon
        }

        gpx.tracks.each do |track|
            track.segments.each do |segment|
                segment.points.each do |point|
                    bounds[:lat_min] = point.lat if  point.lat < bounds[:lat_min]
                    bounds[:lat_max] = point.lat if  point.lat > bounds[:lat_max]
                    bounds[:lon_min] = point.lon if  point.lon < bounds[:lon_min]
                    bounds[:lon_min] = point.lon if  point.lon > bounds[:lon_min]
                end
            end
        end
        bounds
    end
end
