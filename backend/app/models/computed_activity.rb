class ComputedActivity < ApplicationRecord
    belongs_to :activity
    has_one_attached :activity_log_file

end
