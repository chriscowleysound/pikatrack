class User < ActiveRecord::Base
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable
    include DeviseTokenAuth::Concerns::User

    has_many :activities
    has_many :sections

    enum setting_unit: {metric: 0, imperial: 1}
    enum setting_default_privacy: Activity.privacies
    enum setting_default_sport: Activity.activity_types
end
