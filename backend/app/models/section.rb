class Section < ApplicationRecord
    belongs_to :user
    has_many :section_efforts

    enum activity_type: ACTIVITY_TYPES
end
