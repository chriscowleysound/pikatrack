object @activity
attributes :id, :title, :description, :user, :section_efforts, :activity_type
child :user do
  attributes :name, :username, :gender
end
child :section_efforts do
  attributes :section, :time
  child :section do
    attributes :name, :id
  end
end

node(:activity_log_url) { @activity&.activity_file_direct_url }

node(:start_time) { @activity&.computed_activity&.start_time }
node(:end_time) { @activity&.computed_activity&.end_time }
node(:distance) { @activity&.computed_activity&.distance&.round(2) }
node(:moving_time) { @activity&.computed_activity&.moving_time }
node(:elevation) { @activity&.computed_activity&.elevation }
node(:setting_unit) {current_unit}