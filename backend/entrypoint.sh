#!/bin/sh
echo "Waiting for postgres..."

while ! nc -z db 5432; do
  sleep 0.5
done

echo "PostgreSQL started"

cp config/database.yml.example config/database.yml
cp config/secrets.yml.example config/secrets.yml
bundle exec rails db:schema:load
bundle exec unicorn -c config/unicorn.rb
