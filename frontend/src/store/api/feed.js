import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const activities = new Vapi({
    baseURL: process.env.VUE_APP_API_BASE,
    state: {
        activities: []
    }
}).get({
    action: 'get_feed',
    property: 'activities',
    path: '/feed.json'
}).getStore({
    createStateFn: true
})

export default {
    state: activities.state,
    mutations: activities.mutations,
    actions: activities.actions,
    getters: activities.getters
}
